# file:///usr/share/doc/fabric/html/tutorial.html

from fabric.api import local, run, sudo, cd, env
import git

env.hosts = ["tate.debian.org"]


def prepare_deploy():
    #local("./manage.py test my_app")
    #local("git add -p && git commit")
    local("test `git ls-files -cdmu | wc -l` = 0")
    local("git push")


def deploy():
    prepare_deploy()
    repo = git.Repo()
    current_commit = repo.head.commit.hexsha

    deploy_dir = "/srv/debtags.debian.org/debtagsd/"
    with cd(deploy_dir):
        sudo("git fetch", user="debtags")
        sudo("test `git show-ref -s origin/master` = " + current_commit, user="debtags")
        sudo("git rebase origin/master", user="debtags")
        sudo("./manage.py collectstatic --noinput", user="debtags")
        sudo("./manage.py migrate", user="debtags")
        #sudo("psql service=debtags -c 'grant select,insert,update,delete on all tables in schema public to nmweb'",
        #     user="debtags")
        #sudo("psql service=debtags -c 'grant usage on all sequences in schema public to nmweb'", user="debtags")
        sudo("touch debtagsd/wsgi.py", user="debtags")
