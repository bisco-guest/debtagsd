{% for c in checks %}{% with c.js_data as jd %}{% if jd %}{{jd|safe}}{% endif %}{% endwith %}{% endfor %}

// Check a tagset, returning an array of strings with the comments
function check_tags(tags)
{
    var res = new Array();

    function add_check(str)
    {
        var c = new Check("check", str);
        res.push(c);
        return c;
    }

    function add_hint(str)
    {
        var c = new Check("hint", str);
        res.push(c);
        return c;
    }

    {% for c in checks %}{% with c.js_check as jc %}{% if jc %}{{jc|safe}}{% endif %}{% endwith %}{% endfor %}

    return res;
}
