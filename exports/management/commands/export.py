from django.core.management.base import BaseCommand, CommandError
import optparse
from debdata import patches
from backend import models as bmodels
import os
import os.path
import sys
import time
import logging
log = logging.getLogger(__name__)

def do_apriori_rules(**kw):
    """
    List all apriori rules
    """
    rules = bmodels.load_apriori_ruleset()
    if not rules:
        log.warn("apriori rules are not available")
        return

    for rtype in "f", "t", "nf", "nt":
        for r in rules[rtype]:
            print rtype, r.tgt, r.sus, r.conf, ", ".join(r.src)


class Command(BaseCommand):
    help = 'Export information'
    option_list = BaseCommand.option_list + (
#        optparse.make_option("--datadir", action="store", dest="datadir", default="data",
#                             help="Directory with package data"),
#        optparse.make_option("--list-actions", action="store_true", dest="list_actions",
#                             help="List update actions that would be performed"),
#        optparse.make_option("--whitelist", action="store", dest="whitelist", metavar="names",
#                             help="Only perform the actions from this comma or space separated list"),
#        optparse.make_option("--blacklist", action="store", dest="blacklist", metavar="names",
#                             help="Do not perform the actions from this comma or space separated list"),
        optparse.make_option("--quiet", action="store_true", dest="quiet", default=None, help="Disable progress reporting"),
    )

    def handle(self, *args, **options):
        FORMAT = "%(asctime)-15s %(message)s"
        if options["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        if not args:
            print "Please choose what to export from the list below:"
            for k, v in globals().items():
                if k.startswith("do_"):
                    print k[3:], "-", v.__doc__.strip().split("\n")[0]
        else:
            for a in args:
                func = globals().get("do_" + a, None)
                if func is None:
                    print >>sys.stderr, "%s is not a supported dataset" % a
                func(**options)
