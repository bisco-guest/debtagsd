from django.test import TestCase, Client, override_settings
from django.core.urlresolvers import reverse
from backend.unittest import BackendDataMixin

@override_settings(BACKEND_DATADIR="testdata")
class TestView(BackendDataMixin, TestCase):
    def test_index(self):
        client = self.make_test_client()
        response = client.get(reverse("exports_index"))
        self.assertContains(response, "This page document the data export functions of this website.")

    def test_stable_tags(self):
        client = self.make_test_client()
        response = client.get(reverse("exports_stable_tags"))
        self.assertContains(response, "debtags: implemented-in::c++, interface::commandline, role::program, scope::utility, suite::debian, use::searching")
    
    def test_vocabulary_js(self):
        client = self.make_test_client()
        response = client.get(reverse("exports_vocabulary_js"))
        self.assertContains(response, "var fdata = ")
        self.assertEqual(response["Content-Type"], "application/javascript")
    
    def test_tagchecks_js(self):
        client = self.make_test_client()
        response = client.get(reverse("exports_tagchecks_js"))
        self.assertContains(response, "function check_tags(tags)")
        self.assertEqual(response["Content-Type"], "application/javascript")
