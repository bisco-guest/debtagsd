from django.contrib.auth.middleware import RemoteUserMiddleware

class SSOHeaderMiddleware(RemoteUserMiddleware):
    header = 'SSL_CLIENT_S_DN_CN'
