/*
 * debtagsd-specific jquery UI widgets
 *
 * Copyright (C) 2012  Enrico Zini <enrico@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
(function( $ ){
    $.cookie = function (key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && String(value) !== "[object Object]") {
            options = jQuery.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=',
                options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
        return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
    };
})( jQuery );


// End of JQuery cookie plugin, beginning of debtags-ui code

// Persistent settings saved in a cookie
function Settings(options) {
    this.options = $.extend({
        // Cookie where we store the settings
        cookie_name: "settings",
        // Number of days these settings are kept
        expires: 30,
    }, options);
    this._defaults = {};
    this._value = null;
    this._load();
}
Settings.prototype = {
    _load: function() {
        var str = $.cookie(this.options.cookie_name);
        if (!str)
            this._value = {};
        else
            this._value = $.parseJSON(str);
    },
    // Save the settings
    save: function() {
        var str = JSON.stringify(this._value);
        $.cookie(this.options.cookie_name, str, { expires: this.options.expires });
    },
    /*
     * Set some default settings
     *
     * The contents of the given object ({}) are merged into the existing
     * defaults.
     *
     * Defaults are not saved in the settings cookie.
     */
    defaults: function(vals) {
        // Merge these defaults into ours
        $.extend(this._defaults, vals);
    },

    // Get or set a value.
    //
    // This function does not automatically save the settings: save() needs
    // to be called to make changes persistant.
    value: function(key, val) {
        if (arguments.length == 1)
        {
            if (key in this._value)
                return this._value[key];
            else
            {
                var res = this._defaults[key];
                if (res instanceof Array)
                    res = $.extend(true, [], res)
                else if (res instanceof Object)
                    res = $.extend(true, {}, res)
                return this._value[key] = res;
            }
        }
        else
            return this._value[key] = val;
    },
};

// See http://wiki.jqueryui.com/w/page/12138135/Widget%20factory
(function( $ ){
    var global_settings = null;

    /*
     * Return the global singleton settings instance.
     *
     * If it does not exist yet, then it is created with the given options.
     *
     * This allows the settings of a session to be customized by calling
     * $.settings as the first thing in the page onload handler.
     */
    $.settings = function(options) {
        if (global_settings == null)
            global_settings = new Settings(options);
        return global_settings;
    };

    // Patchset editor
    $.widget('ui.patchset', {
        options: {
            // Patchset that we interact with
            patchset: null,

            // Url to which the patchset is submitted
            post_url: null,

            // True if the patchset view is collapsed
            collapsed: false,

	    // Token used for csrf protection
	    csrf_token: null,
        },

        _create: function() {
            var self = this;
            if (self.options.patchset == null)
                throw("patchset must be defined");
            if (self.options.post_url == null)
                throw("post_url must be defined");

            self.element.addClass("dui-patchset");
            self.element.toggleClass("dui-collapsed", self.options.collapsed);

            self._submit = $("<div>").addClass("dui-patchset-submit");
            self.element.append(self._submit);

            var group = $("<span>").addClass("dui-patchset-submit-label");
            self._submit.append(group);

            self._submit_summary = $("<span>").addClass("dui-patchset-submit-summary");
            self._submit_summary.click(function() { self.toggleCollapsed(); });

            self._submit_opener = $("<span>").addClass("dui-patchset-submit-opener").text("(click to edit)");
            self._submit_opener.click(function() { self.toggleCollapsed(); });

            self._submit_spinner = $("<span>").addClass("dui-spinner").addClass("dui-idle");

            group.append(self._submit_summary);
            group.append(" ");
            group.append(self._submit_opener);
            group.append(self._submit_spinner);

            self._submit_button = $("<button>").addClass("dui-patchset-submit-button").text("Submit");
            self._submit_button.bind("click.patchset", function() { self.submit(); });
            self._submit.append(self._submit_button);

            self._submit_messages = $("<div>").addClass("dui-patchset-submit-messages");
            self._submit_messages.append($("<h3>").text("Submission notes"));
            self._submit_messages_list = $("<ul>");
            self._submit_messages.hide();
            self._submit_messages.bind("click.patchset", function() { self._submit_messages.hide(); });
            self._submit_messages.append(self._submit_messages_list);
            self.element.append(self._submit_messages);

            self._pkglist = $("<ul>");
            self.element.append(self._pkglist);

            self.refresh();
        },

        _init: function() {
        //    this.form_close();
        },

        toggleCollapsed: function(val) {
            var self = this;
            if (val == null)
                self.option("collapsed", !self.options.collapsed);
            else
                self.option("collapsed", val);
        },

        _on_remove: function(el) {
            var self = this;
            var el = $(el);
            var pkg = el.parents("li[pkg]").attr("pkg");
            var tag = el.attr("tag");
            self.patch_reset(pkg, tag);
        },

        refresh: function() {
            var self = this;
            var pacthset = self.options.patchset;

            self._pkglist.empty();

            var pkgs = patchset.packages();
            $.each(pkgs, function(idx, el) {
                var pkg = el;
                var patch = patchset.get(pkg);

                var pkgli = $("<li>").attr("pkg", pkg);
                var pkgtitle = $("<span>").addClass("dui-pkgname").text(pkg).bind("click.patchset", function() { self._on_remove(this); });
                var patchlist = $("<ul>");

                pkgli.append(pkgtitle)
                pkgli.append(patchlist);

                patch.each(function(p) {
                    var li = $("<li>").addClass("dui-tagname").attr("tag", p.substr(1)).text(p).bind("click.patchset", function() { self._on_remove(this); });
                    patchlist.append(li);
                });

                self._pkglist.append(pkgli);
            });

            var count_changes = patchset.count_changes();
            if (count_changes > 0)
            {
                if (count_changes == 1)
                    self._submit_summary.text("There is one change to submit:");
                else
                    self._submit_summary.text("There are " + count_changes + " changes to submit:");
                self._submit_button.attr("disabled", false);
                self.element.removeClass("dui-empty");
                self.element.toggleClass("dui-collapsed", self.options.collapsed);
            } else {
                self._submit_summary.text("There are no changes to submit.");
                self._submit_button.attr("disabled", true);
                self.element.removeClass("dui-collapsed");
                self.element.addClass("dui-empty");
            }
        },

        patch: function(pkg, added, removed) {
            var self = this;
            var touched = self.options.patchset.update(pkg, added, removed);
            self.refresh();
            if (touched.length > 0)
                self._trigger("changed", null, { pkg: pkg, tags: touched });
            self._submit_messages.hide();
        },

        patch_reset: function(pkg, tag) {
            var self = this;
            var touched;
            if (tag == null)
            {
                touched = self.options.patchset.reset(pkg);
            } else {
                touched = self.options.patchset.reset(pkg, tag);
            }
            self.refresh();
            if (touched.length > 0)
                self._trigger("changed", null, { pkg: pkg, tags: touched });
            self._submit_messages.hide();
        },

        submit: function() {
            var self = this;
            var patch = self.options.patchset.to_string();
            // If the patch is empty, do nothing
            if (!patch) return;
            var data = {
                "patch": patch,
		"csrfmiddlewaretoken": self.options.csrf_token,
            };

            self._trigger("presubmit", null, data);

            self._submit_messages_list.empty();
            self._submit_messages.hide();
            self._submit_spinner.removeClass("dui-idle");
            self._submit_button.attr("disabled", true);
            var xhr = $.post(submit_url, data, function(data, textStatus, jqXHR) {
                    self._submit_button.attr("disabled", false);
                    self._submit_spinner.addClass("dui-idle");

                    // show remarks
                    if (data.notes.length > 0)
                    {
                        $.each(data.notes, function(idx, el) {
                            self._submit_messages_list.append($("<li>").append(el));
                        });
                        self._submit_messages.show();
                    }

                    self.options.patchset.clear();
                    self.refresh();
                    self._trigger("postsubmit", null, { success: true, data: data, textStatus: textStatus });
                })
                .error(function(jqXHR, textStatus, errorThrown) {
                    self._submit_button.attr("disabled", false);
                    self._submit_spinner.addClass("dui-idle");
                    self._trigger("postsubmit", null, { success: false, data: errorThrown, textStatus: textStatus });
                });
        },

        _setOption: function(key, value) {
            switch (key) {
                case "collapsed":
                    if (!this.element.hasClass("dui-empty"))
                        this.element.toggleClass("dui-collapsed", value);
                    break;
            }
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        destroy: function() {
            $.Widget.prototype.destroy.call(this);
            // TODO
        }
    });


    // Settings editor
    $.widget('ui.settings', {
        options: {
        },

        _create: function() {
            var self = this;

            // Get the singleton settings instance
            self._settings = $.settings();

            // Default settings for what we manage
            self._settings.defaults({
                // Distributions the user is not interested in
                hide_dists: {},
            });

            // Configure hide distro checkboxes
            var hd = $("#hide-dists", self.element);
            var hide_dists = self._settings.value("hide_dists");
            var hide_dist_groups = {};
            var hide_dist_groupers = {};
            function update_grouper(name) {
                // Update the state of the grouper
                var siblings = hide_dist_groups[name];
                var grouper = hide_dist_groupers[name];
                var all_true = true;
                var all_false = true;
                $.each(siblings, function(idx, el) {
                    if (hide_dists[el])
                        all_true = false;
                    else
                        all_false = false;
                });
                if (all_true)
                    grouper.attr("checked", true);
                else if (all_false)
                    grouper.attr("checked", false);
                grouper[0].indeterminate = !(all_true || all_false);
            }
            $("input[type=checkbox]", hd).each(function() {
                var el = $(this);
                var dist = el.attr("id").split("-");
                if (dist.length == 3)
                {
                    // Grouper
                    var subdists = [];
                    hide_dist_groups[dist[2]] = subdists;
                    hide_dist_groupers[dist[2]] = el;
                    el.bind("click.settings", function() {
                        var is_checked = $(this).attr("checked") ? true : false;
                        if (is_checked)
                            $.each(subdists, function(idx, el) { delete hide_dists[el]; });
                        else
                            $.each(subdists, function(idx, el) { hide_dists[el] = true; });
                        $("input[type=checkbox]", $(this).parent()).attr("checked", is_checked);
                        self._trigger("changed", null, [ "hide_dists" ]);
                        self._settings.save();
                    });
                } else {
                    // Leaf item
                    hide_dist_groups[dist[2]].push(dist[3]);
                    var grouper_name = dist[2];
                    dist = dist[3];
                    el.attr("checked", !hide_dists[dist]);
                    el.bind("click.settings", function() {
                        if (el.attr("checked"))
                            delete hide_dists[dist];
                        else
                            hide_dists[dist] = true;

                        // Update the state of the grouper
                        update_grouper(grouper_name);

                        self._trigger("changed", null, [ "hide_dists" ]);
                        self._settings.save();
                    });
                }
            });
            $.each(hide_dist_groups, function(key, val) { update_grouper(key); });

            self.element.dialog({
                title: "Settings",
                autoOpen: false,
                buttons: { "Ok": function() { $(this).dialog("close"); } },
            });
        },

        open: function() {
            this.element.dialog("open");
        },

        _init: function() {
        },

        _setOption: function(key, value) {
            switch (key) {
                case "cookie_name":
                    this._load();
                    break;
                case "expires":
                    this._save();
                    break;
            }
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        destroy: function() {
            $.Widget.prototype.destroy.call(this);
            // TODO
        }
    });
})( jQuery );
