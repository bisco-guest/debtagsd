/*
 * debtagsd editor interface
 *
 * Copyright (C) 2005--2011  Enrico Zini <enrico@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//
// Debtags infrastructure
//

Tagset = function() {
	this._items = {};
	this._size = 0;
	for (var i = 0; i < arguments.length; ++i)
		this.add(arguments[i]);
};
Tagset.prototype = {
	clear: function() {
		this._items = new Object();
		this._size = 0;
	},
  has: function(name) {
		return this._items.hasOwnProperty(name);
	},
	hasRE: function(re) {
		var res = false;
		this.each(function(t) { if(re.exec(t)) res = true; });
		return res;
	},
	add: function(name) {
		if (this.has(name))
			return false;
		this._items[name] = true;
		++this._size;
		return true;
	},
    add_all: function(names) {
		for (i in names)
			this.add(names[i]);
	},
	del: function(name) {
		if (this.has(name))
		{
			delete this._items[name];
			--this._size;
			return true;
		}
		return false;
	},
	size: function() {
		return this._size;
	},
	empty: function() {
		return this._size == 0;
	},
	addFirstTags: function(tags, coll) {
		var lastCard = 0;
		function score(x) { return ((x-15)*(x-15))/x; }

		for (i in tags) {
			var tag = tags[i][0];
			var card = parseInt(tags[i][1]);
			if (i == 0 || score(lastCard) > score(card)) {
				this.add(tag);
				lastCard = card;
			} else
				break;
		}

		// Return always at least the first tag
		if (this.empty() && tags.length > 0)
			this.add(tags[0][0]);
	},
	each: function(fun) {
		for (var i in this._items)
			fun(i);
	},
	merge: function(tset) {
		var tmp=this;
		tset.each(function(i){tmp.add(i)});
	},
	subtract: function(tset) {
		var tmp=this;
		tset.each(function(i){tmp.del(i)});
	},
	contains: function(tset) {
		for (var i in tset._items)
			if (!this.has(i))
				return false;
		return true;
	},
	intersects: function(tset) {
		for (var i in tset._items)
			if (this.has(i))
				return true;
		return false;
	},
    to_list: function() {
        var res = [];
        for (var i in this._items)
            res.push(i);
        return res;
    },
	toString: function() {
		var str = "";
		this.each(function(i){if(str == "") str+=i; else str+=', '+i;});
		return str;
	},
	copy: function() {
		var res = new Tagset();
		this.each(function(i){res.add(i);});
		return res;
	},
	mkpatch: function(ts) {
		var added = ts.copy();
		added.subtract(this);
		var removed = this.copy();
		removed.subtract(ts);
		var res = new Tagset();
		added.each(function(tag){res.add("+"+tag);});
		removed.each(function(tag){res.add("-"+tag);});
		return res;
	}
	/*
	filtervoc: function(voc) {
		var voc1 = new Vocabulary();
		// TODO: return a vocabulary with only those tags in voc that are also in
		// this tagset
	}
	*/
};

Vocabulary = function(fdata, tdata) {
    this.facs = fdata;
    this.tags = tdata;
    this.ftags = {};
    for (var i in this.tags) {
        var tmp = i.split("::");
        if (! this.ftags.hasOwnProperty(tmp[0]))
            this.ftags[tmp[0]] = new Tagset();
        this.ftags[tmp[0]].add(i);
    }
};
Vocabulary.prototype = {
    // Return an array [shortdesc, longdesc]
    facData: function(fac) {
        res = this.facs[fac];
        if (res == null) res = [fac, fac];
        return res;
    },
    // Return an array [shortdesc, longdesc]
    tagData: function(tag) {
        res = this.tags[tag];
        if (res == null) res = [tag, tag];
        return res
    },
    facetOfTag: function(tag) {
        return tag.split("::")[0];
    },
	tagsOfFacet: function(fac) {
		return this.ftags[fac];
	},
    eachFacet: function(fun) {
        var facets = [];
        for (var i in this.facs)
            facets.push(i);
        facets.sort();

        for (var i in facets)
        {
            var facet = this.facs[facets[i]];
            fun(facets[i], facet[0], facet[1]);
        }
    },
	eachTag: function(fun) {
		for (var i in this.tags) {
			fun(i, this.tags[i][0], this.tags[i][1]);
		}
	},
    eachTagOfFacet: function(fac, fun) {
        var ts = this.tagsOfFacet(fac);
        var tags = [];
        if (ts) ts.each(function(t) { tags.push(t); });
        tags.sort();

        var tmp = this;
        for (var i in tags)
        {
            var tag = tags[i];
            fun(tag, tmp.tags[tag][0], tmp.tags[tag][1]);
        }
    }
};

Patch = function() {
    this.added = {};
    this.removed = {};
};
Patch.prototype = {
    add: function(tag) {
        this.added[tag] = true;
        delete this.removed[tag];
    },
    del: function(tag) {
        this.removed[tag] = true;
        delete this.added[tag];
    },
    update: function(added, removed) {
        // Scan for items common to both sets
        var seen = {};
        var common = {};
        for (var i in added) { seen[added[i]] = true; }
        for (var i in removed) { if (seen[removed[i]]) common[removed[i]] = true; }

        // Perform changes, taking note of what actually changed
        var touched = [];
        for (var i in added)
        {
            var t = added[i];
            if (common[t]) continue;
            if (!this.added[t])
            {
                this.added[t] = true;
                touched.push(t);
            }
            if (this.removed[t])
            {
                delete this.removed[t];
                touched.push(t);
            }
        }
        for (var i in removed)
        {
            var t = removed[i];
            if (common[t]) continue;
            if (!this.removed[t])
            {
                this.removed[t] = true;
                touched.push(t);
            }
            if (this.added[t])
            {
                delete this.added[t];
                touched.push(t);
            }
        }
        return touched;
    },
    reset: function(tag) {
        if (this.added[tag] || this.removed[tag])
        {
            delete this.added[tag];
            delete this.removed[tag];
            return true;
        } else
            return false;
    },
    has: function(tag) {
        return this.added[tag] || this.removed[tag];
    },
    get: function(tag) {
        if (this.added[tag]) return "+";
        if (this.removed[tag]) return "-";
        return null;
    },
    sorted: function() {
        var res = [];
        for (var i in this.added)
            res.push("+" + i);
        for (var i in this.removed)
            res.push("-" + i);
        res.sort();
        return res;
    },
    each: function(f) {
        var all = this.sorted();
        for (var i in all)
        {
            f(all[i]);
        }
    },
    to_string: function() {
        return this.sorted().join(", ");
    },
    is_empty: function() {
        for (var i in this.added)
            return false;
        for (var i in this.removed)
            return false;
        return true;
    },
    is_added: function(tag) {
        return this.added[tag] == true;
    },
    is_removed: function(tag) {
        return this.removed[tag] == true;
    },
    count_changes: function() {
        var res = 0;
        for (var i in this.added)
            ++res;
        for (var i in this.removed)
            ++res;
        return res;
    },
    all_tags: function() {
        var res = [];
        for (var i in this.added)
            res.push(i);
        for (var i in this.removed)
            res.push(i);
        return res;
    },
    /**
     * Returns true if patch is a subset of this patch
     */
    contains: function(patch) {
        var self = this;
        for (var t in patch.added)
            if (!self.added[t])
                return false;
        for (var t in patch.removed)
            if (!self.removed[t])
                return false;
        return true;
    },
    /**
     * Returns true if patch conflicts with this patch
     */
    conflicts: function(patch) {
        var self = this;
        for (var t in patch.added)
            if (self.removed[t])
                return true;
        for (var t in patch.removed)
            if (self.added[t])
                return true;
        return false;
    },
};

PatchSet = function() {
    this.patches = {};
};
PatchSet.prototype = {
    /**
     * Iterate the patchset, calling f("pkg", patch) for each entry
     */
    each: function(f) {
        var self = this;
        $.each(self.patches, f);
    },
    /**
     * Evaluate f("pkg", patch) on all entries.
     *
     * Return true if f is always true, false if f is false at least once.
     * Short circuiting: exits as soon as f is false.
     *
     * Returns undefined if this patchset is empty.
     */
    pred_is_true: function(f) {
        var self = this;
        var res = undefined;
        $.each(self.patches, function(pkg, patch) {
            if (!f(pkg, patch))
            {
                res = false;
                return false;
            }
            res = true;
        });
        return res;
    },
    /**
     * Evaluate f("pkg", patch) on all entries.
     *
     * Return true if f is always false, false if f is true at least once.
     * Short circuiting: exits as soon as f is true.
     *
     * Returns undefined if this patchset is empty.
     */
    pred_is_false: function(f) {
        var self = this;
        var res = undefined;
        $.each(self.patches, function(pkg, patch) {
            if (f(pkg, patch))
            {
                res = false;
                return false;
            }
            res = true;
        });
        return res;
    },
    update: function(pkg, added, removed) {
        var patch = this.patches[pkg];
        if (patch == null)
        {
            patch = new Patch();
            this.patches[pkg] = patch;
        }
        var touched = patch.update(added, removed);
        if (patch.is_empty())
            delete this.patches[pkg];
        return touched;
    },
    /// Update this patchset using all the entries of ps
    update_with_patchset: function(ps) {
        var self = this;
        ps.each(function(pkg, patch) {
            self.update(pkg, Object.keys(patch.added), Object.keys(patch.removed));
        });
    },
    reset: function(pkg, tag) {
        if (tag == null)
        {
            if (this.patches[pkg] != null)
            {
                var res = this.patches[pkg].all_tags();
                delete this.patches[pkg];
                return res;
            } else
                return [];
        }
        else
        {
            var p = this.patches[pkg];
            if (p == null) return [];
            var changed = p.reset(tag);
            if (p.is_empty())
                delete this.patches[pkg];
            return changed ? [tag] : [];
        }
    },
    clear: function() {
        this.patches = {};
    },
    get: function(pkg, tag) {
        var patch = this.patches[pkg];
        if (tag == null)
            return patch;
        else if (patch == null)
            return null;
        else
            return patch.get(tag);
    },
    has: function(pkg, tag) {
        var patch = this.patches[pkg];
        if (tag == null)
            return patch != null;
        else if (patch == null)
            return false;
        else
            return patch.has(tag);
    },
    // Return a sorted list of packages involved in this patch
    packages: function() {
        var pkgs = [];
        for (var pkg in this.patches)
            pkgs.push(pkg);
        pkgs.sort();
        return pkgs;
    },
    // Return a sorted list of tags involved in this patch
    tags: function() {
        // Build a set with all tags touched by this patchset
        var tags = {};
        $.each(this.patches, function(pkg, patch) {
            for (var t in patch.added)
                tags[t] = true;
            for (var t in patch.removed)
                tags[t] = true;
        });
        // Turn the set into a list
        var res = [];
        for (var t in tags)
            res.push(t);
        // Sort and return it
        res.sort();
        return res;
    },
    to_string: function() {
        var res = [];
        var pkgs = this.packages();
        for (var i in pkgs)
            res.push(pkgs[i] + ": " + this.patches[pkgs[i]].to_string());
        return res.join("\n");
    },
    is_added: function(pkg, tag) {
        var p = this.get(pkg);
        if (p == null) return false;
        return p.is_added(tag);
    },
    is_removed: function(pkg, tag) {
        var p = this.get(pkg);
        if (p == null) return false;
        return p.is_removed(tag);
    },
    count_changes: function() {
        var res = 0;
        for (var pkg in this.patches)
            res += this.patches[pkg].count_changes();
        return res;
    },
    /**
     * Returns true if patchset ps is a subset of this patchset
     */
    contains: function(ps) {
        var self = this;

        var res = ps.pred_is_true(function(pkg, patch) {
            if (!self.patches[pkg])
                return false;
            if (!self.patches[pkg].contains(patch))
                return false;
            return true;
        });

        if (res === undefined)
            return $.isEmptyObject(ps);
        return res;
    },

    /**
     * Returns true if patchset ps conflicts with this patchset
     */
    conflicts: function(ps) {
        var self = this;

        var res = ps.pred_is_true(function(pkg, patch) {
            if (!self.patches[pkg])
                return true;
            var res = !self.patches[pkg].conflicts(patch);
            return res;
        });

        if (res === undefined)
            return false;
        return !res;
    },

    /**
     * Return the inverse of this patchset
     */
    inverted: function() {
        var self = this;
        var res = new PatchSet();
        self.each(function(pkg, patch) {
            res.update(pkg, Object.keys(patch.removed), Object.keys(patch.added));
        });
        return res;
    },

    /**
     * Call f(pkg, [tags]) for each conflicting package between
     * this patchset and ps.
     *
     * tags contains the tags for which ps has different opinions from this
     * patchset.
     */
    foreach_conflict: function(ps, f) {
        var self = this;
        self.each(function(pkg, patch) {
            var tags = [];

            var p = ps.patches[pkg];
            if (p)
            {
                for (var t in patch.added)
                    if (p.removed[t])
                        tags.push(t);
                for (var t in patch.removed)
                    if (p.added[t])
                        tags.push(t);

                if (tags.length)
                    f(pkg, tags);
            }
        });
    },
};

Check = function(type, msg) {
    this.type = type;
    this.msg = msg;
    this.fixes = [];
};
Check.prototype = {
    add_fix: function(msg, patch) {
        this.fixes.push({ msg: msg, patch: patch });
    },
};

function facet_of_tag(tag) {
    return tag.split("::")[0];
}

// vim:set ts=2 sw=2:
