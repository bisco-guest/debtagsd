# coding: utf-8
from django import http
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
import json
import datetime
from checks import checks
from backend.utils import add_cors_headers
from backend import models as bmodels
from reports import models as rmodels
from collections import defaultdict
import calendar


class Stats(TemplateView):
    template_name = "reports/stats.html"

    def get_context_data(self, **kw):
        ctx = super(Stats, self).get_context_data(**kw)

        from django.db.models import Max
        from django.db import connection
        truncate_date = connection.ops.date_trunc_sql('month', 'ts')
        history = []
        for s in bmodels.Stats.objects.extra(
                {"month": truncate_date}).values("month").annotate(
                        trivial=Max("count_trivial"), robots=Max("count_robots"), humans=Max("count_humans"), tags=Max("count_tags")).order_by("month"):
            if isinstance(s["month"], datetime.datetime):
                ts = calendar.timegm(s["month"].utctimetuple())
            else:
                ts = calendar.timegm(datetime.datetime.strptime(s["month"], "%Y-%m-%d").utctimetuple())
            history.append((
                ts,
                s["trivial"],
                s["robots"],
                s["humans"],
                s["tags"]))

        stats = bmodels.tag_stats()

        check_stats = []
        for check_id, count in bmodels.TodoEntry.stats():
            check = checks.CheckEngine.by_id(check_id)
            check_stats.append((count, check))
        check_stats.sort(reverse=True)

        ctx.update(
            history=json.dumps(history),
            stats=stats,
            checks=check_stats,
        )

        return ctx


class TodoView(TemplateView):
    template_name = "reports/todo.html"

    def get_context_data(self, **kw):
        ctx = super(TodoView, self).get_context_data(**kw)
        # Top packages with todo entries, sorted by popcon
        ctx["pkgs"] = bmodels.Package.objects.exclude(todolist=None).order_by("-popcon")[:50]
        return ctx


class TodoMaintView(TemplateView):
    template_name = "reports/maint.html"

    def get_context_data(self, **kw):
        ctx = super(TodoMaintView, self).get_context_data(**kw)
        maint = get_object_or_404(bmodels.Maintainer, email=self.kwargs["email"])
        sources = defaultdict(list)
        for mp in bmodels.MaintPkg.objects.filter(maint=maint).select_related("pkg").order_by("pkg__name"):
            sources[mp.pkg.source].append((mp, bmodels.PkgExtradir(mp.pkg.name)))
        ctx["maint"] = maint
        ctx["sources"] = sorted(sources.items())
        return ctx


def pkginfo_view(request, name):
    pkg = bmodels.Package.by_name(name)
    if pkg is None:
        return http.HttpResponseNotFound("Package %s was not found" % name)
    extra = bmodels.PkgExtradir(name)

    return render(request, "reports/package.html", dict(pkg=pkg, extra=extra))


def facets(request):
    facets = bmodels.Facet.objects.all().order_by("name")

    # Compute statistics
    sstats = dict()
    for name, card in bmodels.Tag.card_stats():
        fname = name.split("::")[0]
        sstats[fname] = sstats.get(fname, 0) + card

    facets = [(f, sstats.get(f.name, 0)) for f in facets]

    return render(request, "reports/facets.html",
                  dict(facets=facets, sstats=sstats))


def facet(request, name):
    facet = bmodels.Facet.by_name(name)
    if facet is None:
        return http.HttpResponseNotFound("Facet %s was not found" % name)

    # Compute statistics
    facpfx = name + "::"
    sstats = dict((name, card) for name, card in bmodels.Tag.card_stats() if name.startswith(facpfx))
    scount = sum(sstats.values())
    tags = [(t, sstats.get(t.name, 0)) for t in facet.tags.order_by("name")]

    return render(request, "reports/facet.html",
                  dict(facet=facet, scount=scount, sstats=sstats, tags=tags,))


def taginfo_view(request, name):
    tag = bmodels.Tag.by_name(name)
    if tag is None:
        return http.HttpResponseNotFound("Tag %s was not found" % name)
    return render(request, "reports/tag.html", dict(tag=tag))


def tag_count_chart(request, ttype):
    """
    Create a graph of count of packages by number of tags, with bars splitting
    robot/human maintained
    """
    res = rmodels.make_tag_count_chart(ttype=ttype)

    if res is None:
        response = http.HttpResponseNotFound("Function not implemented: needs python-pychart to be installed")
        add_cors_headers(response)
        return response
    else:
        response = http.HttpResponse(content_type="image/png")
        add_cors_headers(response)
        response.write(res)
        return response


def list_checks(request):
    from checks import checks

    pkgcount = bmodels.Package.objects.count()
    check_stats = []
    for check_id, count in bmodels.TodoEntry.stats():
        check = checks.CheckEngine.by_id(check_id)
        check_stats.append((count, check))
    check_stats.sort(key=lambda x: x[1].ID)

    return render(request, "reports/list-checks.html",
                  dict(
                      pkgcount=pkgcount,
                      checks=check_stats,
                  ))


def show_check(request, id):
    from checks import checks
    id = int(id)
    check = checks.CheckEngine.by_id(id)
    if check is None:
        return http.HttpResponseNotFound("Check with id %d was not found" % id)

    # Top packages with todo entries of the given check, sorted by popcon
    pkgs = [pkg for pkg in bmodels.Package.objects.filter(todolist__check_id=check.ID).order_by("-popcon")[:50]]

    return render(request, "reports/show-check.html",
                  dict(
                      check=check,
                      pkgs=pkgs,
                  ))


class AuditLog(TemplateView):
    template_name = "reports/auditlog.html"

    def get_context_data(self, **kw):
        ctx = super(AuditLog, self).get_context_data(**kw)

        if self.request.user.is_anonymous:
            raise PermissionDenied

        ctx["audit_log"] = self.request.user.audit_log.all().order_by("-ts")

        return ctx


def apriori_rules(request):
    rules = bmodels.load_apriori_ruleset()
    if not rules:
        return http.HttpResponseNotFound("apriori rules are not available")

#    for rtype in "f", "t", "nf", "nt":
#        for r in rules[rtype]:
#            print rtype, r.tgt, r.sus, r.conf, ", ".join(r.src)

    return render(request, "reports/apriori-rules.html",
                  dict(
                      rules=rules,
                  ))


def tags_for_maintainer(request):
    from backend.utils import add_cors_headers

    # Get list of maintainers to work on
    todo = []
    for k, v in request.GET.items():
        emails = json.loads(v)
        if not isinstance(emails, list):
            continue
        todo.append((k, emails))

    # Count of all packages
    pkg_count = bmodels.Package.objects.count()

    # Global tag cardinalities
    tag_cards = dict(bmodels.Tag.card_stats())

    res = {}
    for key, emails in todo:
        # Parse and complete emails
        maints = []
        for e in emails:
            if "@" in e:
                addr = e
            else:
                addr = e + "@debian.org"

            try:
                m = bmodels.Maintainer.objects.get(email=addr)
                maints.append(m)
            except bmodels.Maintainer.DoesNotExist:
                pass

        # From maintainers to packages
        pkgs = bmodels.Package.objects.filter(maintainers__in=maints)

        # From packages to tags
        tcounts = dict()
        for p in pkgs:
            for t in p.stable_tags.all():
                if t.name in tcounts:
                    tcounts[t.name]["count"] += 1
                else:
                    tcounts[t.name] = dict(count=1)

        # Add global tag cardinalities and TF/IDF scores
        for k, v in tcounts.items():
            v["tot"] = tag_cards.get(k, 0)
            # v["tfidf"] = "%.2f" % (v["count"] * math.log(pkg_count / v["tot"]))

        res[key] = tcounts

    res = http.HttpResponse(
        json.dumps(dict(
            pkg_count=pkg_count,
            maints=res)),
        content_type="application/json")
    add_cors_headers(res)
    return res


def recent_view(request):
    date_from = request.GET.get("from", None)
    if date_from is None:
        date_from = datetime.date.today() - datetime.timedelta(days=7)
    else:
        date_from = datetime.datetime.strptime(date_from, "%Y-%m-%d")
    pkgs = bmodels.Package.objects.order_by("-ts_created")
    pkgs = pkgs.filter(ts_created__gte=date_from)
    pkgs = [pkg.to_dict() for pkg in pkgs]
    return render(request, "reports/recent.html", dict(pkgs=pkgs, date_from=date_from))
