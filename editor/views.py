# coding: utf-8
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django import http
from django.shortcuts import redirect
from django.views.generic import TemplateView
import json
from backend import models as bmodels
from aptxapianindex.axi import AptXapianIndex

class Index(TemplateView):
    template_name = "editor/index.html"

    def get(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            return redirect("root_doc_sso")
        name = request.GET.get("pkg", "").strip()
        if not name:
            return super(Index, self).get(request, *args, **kw)
        else:
            return redirect("editor_edit", name=name)


class Edit(TemplateView):
    template_name = "editor/edit.html"

    def dispatch(self, request, *args, **kw):
        if self.request.user.is_anonymous:
            return redirect("root_doc_sso")

        name = kw["name"]
        pkg = bmodels.Package.by_name(name)
        if pkg is None:
            return http.HttpResponseNotFound("Package %s was not found" % name)
        if pkg.trivial:
            raise PermissionDenied("trivial packages cannot be edited")
        self.package = pkg

        return super(Edit, self).dispatch(request, *args, **kw)

    def get_context_data(self, **kw):
        ctx = super(Edit, self).get_context_data(**kw)

        # Compute tag suggestions
        axi = AptXapianIndex()
        suggested_tags = json.dumps(list(axi.suggest_tags(self.package, count=15)))

        ctx.update(
            pkg=self.package,
            tags=json.dumps([x.name for x in self.package.stable_tags.all()]),
            suggested_tags=suggested_tags,
        )

        return ctx
