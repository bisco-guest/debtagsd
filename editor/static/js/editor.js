/*
 * debtagsd editor interface
 *
 * Copyright (C) 2011  Enrico Zini <enrico@debian.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Package we are editing
var pkg = false;

// Facet and tag information
var voc = new Vocabulary(fdata, tdata);

// Tags of the package, before editing
var origtags = new Tagset();

// Tags of the package, edited
var tags = origtags.copy();

// Currently displayed view
var curtagpanel = "sugg";

// Currently selected facet in the all tags view
var atcurfacet = "role";

// List of suggested tags for this package
var suggested = [];

// Results of the tag search
var relevant = [];

// URL used to submit patches
var submit_url = "https://debtags.debian.org/api/patch";

// URL used to submit patches
var search_tags_url = "https://debtags.debian.org/tag/search";

function curtag_tooltip_handler()
{
    var tag = $(this).attr("tag");
    var finfo = voc.facData(voc.facetOfTag(tag));
    var tinfo = voc.tagData(tag);
    var res = "<div class='tooltip'>"
    res += "Facet: <b>" + finfo[0] + "</b>";
    if (finfo[1])
        res += "<p>"+finfo[1]+"</p>";
    res += "Tag: <b>" + tinfo[0] + "</b>";
    if (tinfo[1])
        res += "<p>"+tinfo[1]+"</p>";
    res += "</div>";
    return res;
}
function curtag_hooks(el)
{
    el.tooltip({
        bodyHandler: curtag_tooltip_handler,
    })
    return el;
}

// Add a tag to the package
function add_tag(name)
{
    $("#patchset").patchset("patch", pkg, [name], []);
}

// Remove a tag from the package
function del_tag(name)
{
    $("#patchset").patchset("patch", pkg, [], [name]);
}

// Called when a .tag is clicked that should toggle a tag
function do_tag_toggle(ev)
{
    var el = $(this);
    var tag = el.attr("tag");
    if (tags.has(tag))
        del_tag(tag);
    else
        add_tag(tag);
}

// Called when a .tag is clicked that should add the tag
function do_tag_add(ev)
{
    var el = $(this);
    var tag = el.attr("tag");
    add_tag(tag);
}

// Called when a .tag is clicked that should delete the tag
function do_tag_del(ev)
{
    var el = $(this);
    var tag = el.attr("tag");
    del_tag(tag);
}

// Render a tag with the given name, returning the DOM object for it
function render_tag(tname)
{
    var tinfo = tdata[tname];
    if (tinfo == null)
    {
        tinfo = ["(no description)", null];
    }
    var span = $("<span>").addClass("tag").addClass("menu").attr("tag", tname)
                          .append($("<span>").addClass("name").text(tname))
                          .append(": ")
                          .append($("<span>").addClass("desc").append(tinfo[0]));
    if (tinfo[1])
        span.append($("<div>").addClass("ldesc").append(tinfo[1]));

    if (tags.has(tname))
        span.addClass("current");

    span.click(do_tag_toggle);

    return span;
}

// Render a tag for the current tag list
function render_selected_tag(tname)
{
    var span = $("<span>").addClass("tag").attr("tag", tname)
                          .append($("<span>").addClass("name").text(tname));
    span.click(do_tag_del);
    return span;
}

function on_fix_clicked()
{
    var el = $(this);
    var patch = el.attr("patch");
    $.each(patch.split(", "), function(idx, el) {
        if (el[0] == "+") {
            add_tag(el.substr(1));
        } else if (el[0] == "-") {
            del_tag(el.substr(1));
        }
    });
}

function update_tips()
{
    var comments = commentTagset(pkg, tags);
    var todolist = $("#todolist");
    var todolist_list = $("ul", todolist);
    var has_any = false;

    todolist_list.empty();
    if (comments.length > 0)
    {
        $.each(comments, function(idx, c) {
            var entry = $("<li>").addClass(c.type).append(c.msg);
            if (c.fixes.length > 0)
            {
                var fixlist = $("<ul>").addClass("fixes");
                $.each(c.fixes, function(idx, f) {
                    var fixentry = $("<li>").attr("patch", f.patch).html(f.msg);
                    fixlist.append(fixentry);
                    fixentry.click(on_fix_clicked);
                })
                entry.append(fixlist);
            }
            todolist_list.append(entry);
        });
        has_any = true
    }

    if (has_any)
        todolist.show();
    else
        todolist.hide();

}

function alltags_facet_selected()
{
    var el = $(this);
    var facet = el.attr("facet");
    atcurfacet = facet;
    update_available();
}

// Update visibility in the list of available tags
function update_available()
{
    var at_facs = $("#available_facets_list");
    var at_tags = $("#available_tags_list");
    var tagfilter = document.alltags.tagsearch.value.toLowerCase();
    var facfilter = atcurfacet + "::";

    // Mark the current facet as current
    $("#available_facets_list li.current").removeClass("current");
    $("#available_facets_list li[facet=\""+atcurfacet+"\"]").addClass("current");

    // View the name of the currently selected facet
    var facDesc = voc.facData(atcurfacet);
    $("#available_selected_facet").text(facDesc[0]);

    // Update tag visibility
    var facet_visible = {};
    $(".tag", at_tags).each(function(idx, el) {
        var el = $(el);
        var tag = el.attr("tag");
        var taginfo = tdata[tag];
        var visible = true;

        if (tagfilter)
        {
            var text = (tag + " " + taginfo[0] + " " + taginfo[1]).toLowerCase();
            if (text.indexOf(tagfilter) == -1)
                visible = false;
        }

        if (visible)
            // Mark this facet as visible
            facet_visible[tag.split("::")[0]] = true;

        if (visible && tag.slice(0, facfilter.length) != facfilter)
            visible = false;

        if (visible)
            el.parent().show();
        else
            el.parent().hide();
    });

    $("li", at_facs).each(function(idx, el) {
        var el = $(el);
        var facet = el.attr("facet");
        if (facet_visible[facet])
            el.show();
        else
            el.hide();
    });
}

function show_random_tip()
{
    var idx = Math.round(Math.random() * tips.length);
    while (idx == tips.length)
        idx = Math.round(Math.random() * tips.length);
    var tipbox = $("#tip");
    tipbox.empty();
    tipbox.append(tips[idx]);
}

function update_visibility(curbox)
{
    if (curbox)
        curtagpanel = curbox;

    $("#alltags").toggle(curtagpanel == "all");
    $(".allButton").toggleClass("current", curtagpanel == "all");

    $("#suggtags").toggle(curtagpanel == "sugg");
    $(".suggButton").toggleClass("current", curtagpanel == 'sugg');

    $("#reltags").toggle(curtagpanel == "rel");
    $(".relButton").toggleClass("current", curtagpanel == 'rel');
}

function reset_tags(new_tags)
{
    origtags = new Tagset();
    origtags.add_all(new_tags);
    tags = origtags.copy();

    // Render the list of current tags
    var current_tag_list = $("#current_tag_list");
    current_tag_list.empty();
    count = 0;
    tags.each(function(t) {
        current_tag_list.append($("<li>").append(curtag_hooks(render_selected_tag(t))));
        count += 1;
    });
    if (count == 0)
        current_tag_list.append($("<li>").addClass("no_tags").append("None"));

    // Update the visibility of all tags
    $(".menu.tag").each(function (idx, el) {
        var el = $(el);
        var tag = el.attr("tag");
        if (tags.has(tag))
            el.addClass("current");
        else
            el.removeClass("current");
    });
}

function search_tags(query)
{
    if (query == null)
        query = $("#tag_search_query").val();

    var search_tags_list = $("#search_tags_list");
    search_tags_list.empty();

    document.relsearch.keys.disabled = true;
    document.relsearch.submit.disabled = true;

    $.getJSON(search_tags_url + encodeURIComponent(query), null,
        function(data, textStatus, jqXHR) {
            document.relsearch.keys.disabled = false;
            document.relsearch.submit.disabled = false;
            $.each(data.tags, function(idx, el) {
                var tag_el = render_tag(el.n);
                tag_el.tooltip({
                    bodyHandler: curtag_tooltip_handler,
                })
                search_tags_list.append($("<li>").append(tag_el));
            });
        })
        .error(function(jqXHR, textStatus, errorThrown) {
            alert("Tag search failed: " + textStatus + " " + errorThrown);
            document.relsearch.keys.disabled = false;
            document.relsearch.submit.disabled = false;
        });
}

// From http://www.quirksmode.org/js/cookies.html
function readCookie(name)
{
   var nameEQ = name + "=";
   var ca = document.cookie.split(';');
   for (var i=0; i < ca.length; ++i)
   {
       var c = ca[i];
       while (c.charAt(0)==' ') c = c.substring(1, c.length);
       if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
   }
   return null;
}

// TODO: besides the hasCookies check, generate the rest from the checks module

var hasCookies = readCookie("patchtag") != null;

function commentTagset(pkg, tags)
{
	var res = check_tags(tags);
	return res;
}
