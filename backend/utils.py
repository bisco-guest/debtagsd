# debtagsd website backend
#
# Copyright (C) 2011  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Utility functions available to all apps running on this backend
"""

def add_cors_headers(response):
    """
    Add CORS headers to a response, so that javascript apps can access it from
    anywhere.

    See http://en.wikipedia.org/wiki/Cross-Origin_Resource_Sharing
    """
    response['Access-Control-Allow-Origin'] = "*"
    response['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    response['Access-Control-Max-Age'] = 1000
    response['Access-Control-Allow-Headers'] = '*'
    return response

