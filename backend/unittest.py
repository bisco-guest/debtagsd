from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import division
from django.test import TestCase, override_settings, Client
from debian import debtags
import backend.models as bmodels
import os
import os.path
import shutil


class BackendDataMixin(object):
    @classmethod
    def build_test_db(cls, stages=("main",)):
        cls.user = bmodels.User.objects.create_user("test")

        with override_settings(BACKEND_DATADIR="testdata", DEBTAGS_HOUSEKEEPING_LOCAL=True):
            # FIXME: this is run before every test method and is SLOW, considering
            # that all test methods are readonly. There seems to be no support at
            # the moment for loading readonly test data only once
            from django_housekeeping import Housekeeping
            hk = Housekeeping()
            hk.autodiscover()
            hk.init()
            for stage in stages:
                hk.stages[stage].run()
    
        tag_objects = { x.name: x for x in bmodels.Tag.objects.all() }

        db = debtags.DB()
        with open("testdata/tags-stable", "r") as fd:
            db.read(fd)
        for pkg, tags in db.iter_packages_tags():
            pkg = bmodels.Package.objects.get(name=pkg)
            pkg.stable_tags = [ tag_objects.get(tag) for tag in tags ]

    @classmethod
    def _clear_filesystem(cls):
        for name in "pkgs",:
            pathname = os.path.join("testdata", name)
            if os.path.isdir(pathname):
                shutil.rmtree(pathname)

    @classmethod
    def setUp(cls):
        cls._clear_filesystem()
        cls.build_test_db()

    @classmethod
    def tearDown(cls):
        bmodels.User.objects.all().delete()
        bmodels.Package.objects.all().delete()
        bmodels.TodoEntry.objects.all().delete()
        bmodels.Facet.objects.all().delete()
        bmodels.Tag.objects.all().delete()
        bmodels.Maintainer.objects.all().delete()
        bmodels.MaintPkg.objects.all().delete()
        bmodels.AuditLog.objects.all().delete()
        bmodels.Stats.objects.all().delete()
        cls._clear_filesystem()

    def make_test_client(self, user=None, **kw):
        """
        Instantiate a test client, logging in the given person.

        If person is None, visit anonymously.
        """
        if user is not None:
            kw["SSL_CLIENT_S_DN_CN"] = user.username
        client = Client(**kw)
        return client
